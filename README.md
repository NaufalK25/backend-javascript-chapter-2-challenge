# Backend JavaScript Chapter 2 Challenge

Challenge for Chapter 2 from Binar Academy - Backend JavaScript class

### Prerequisites

1. [Git](https://git-scm.com/downloads)
    ```
    git --version
    ```
2. [Node.js](https://nodejs.org/en/)
    ```
    node -v
    ```

### Run In Terminal

1. Clone the repository

    ```
    git clone https://gitlab.com/NaufalK25/backend-javascript-chapter-2-challenge.git
    ```

2. Run using node command

    ```
    npm start
    ```

### Questions

Simple grading system

1. [Count the students](src/Math.js)
2. [Get max and min score](src/Math.js)
3. [Get average score](src/Math.js)
4. [Count the students who passed and failed](src/Grade.js)
5. [Sort the student score by ascending and descending order](src/Grade.js)

### Flowchart

<style>
.flowchart {
  background-color: #fff;
  text-align: center;
  padding: 1em;
}
</style>

1.  [Main program](flowchart/img/index.png)

    <div class="flowchart">
        <img src="flowchart/svg/index.svg" alt="index" />
    </div>

2.  [Max function](flowchart/img/max.png)

    <div class="flowchart">
        <img src="flowchart/svg/max.svg" alt="max" />
    </div>

3.  [Min function](flowchart/img/min.png)

    <div class="flowchart">
        <img src="flowchart/svg/min.svg" alt="min" />
    </div>

4.  [Avg function](flowchart/img/avg.png)

    <div class="flowchart">
        <img src="flowchart/svg/avg.svg" alt="avg" />
    </div>

5.  [Pass function](flowchart/img/pass.png)

    <div class="flowchart">
        <img src="flowchart/svg/pass.svg" alt="pass" />
    </div>

6.  [Fail function](flowchart/img/fail.png)

    <div class="flowchart">
        <img src="flowchart/svg/fail.svg" alt="fail" />
    </div>

7.  [Sort function](flowchart/img/sort.png)

    <div class="flowchart">
        <img src="flowchart/svg/sort.svg" alt="sort" />
    </div>
