module.exports = class Math {
    static _PRECISION = 2;

    constructor(numbers = []) {
        this.numbers = numbers;
        this.count = numbers.length;
    }

    max() {
        let maxNumber = this.numbers[0];

        for (let i = 1; i < this.numbers.length; i++) {
            if (this.numbers[i] > maxNumber) {
                maxNumber = this.numbers[i];
            }
        }
        return maxNumber || '-';
    }

    min() {
        let minNumber = this.numbers[0];

       for (let i = 1; i < this.numbers.length; i++) {
           if (this.numbers[i] < minNumber) {
               minNumber = this.numbers[i];
           }
       }
        return minNumber || '-';
    }

    avg() {
        let total = 0;

        for (let i = 0; i < this.numbers.length; i++) {
            total += this.numbers[i];
        }
        return (this.count > 0) ? (total / this.count).toFixed(Math._PRECISION) : '-';
    }
}
