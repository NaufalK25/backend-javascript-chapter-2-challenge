const colorizeConsole = (colorCode, string) => {
    return `\x1b[${colorCode}m${string}\x1b[0m`;
}

module.exports = {
    colorizeConsole,
};
