const Math = require('./Math');

module.exports = class Grade extends Math {
    static PASSING_GRADE = 75;

    constructor(name = '-', grades = []) {
        super(grades);
        this.name = name;
        this.grades = this.numbers;
    }

    pass() {
        let passed = 0;

        for (let i = 0; i < this.grades.length; i++) {
            if (this.grades[i] >= Grade.PASSING_GRADE) {
                passed++;
            }
        }
        return passed;
    }

    fail() {
        return this.count - this.pass();
    }

    sort(order) {
        for (let i = 0; i < this.count; i++) {
            for (let j = 0; j < this.count - i - 1; j++) {
                if (order === 'asc') {
                    if (this.grades[j] > this.grades[j + 1]) {
                        let temp = this.grades[j];
                        this.grades[j] = this.grades[j + 1];
                        this.grades[j + 1] = temp;
                    }
                } else if (order === 'desc') {
                    if (this.grades[j] < this.grades[j + 1]) {
                        let temp = this.grades[j];
                        this.grades[j] = this.grades[j + 1];
                        this.grades[j + 1] = temp;
                    }
                } else {
                    return 'Invalid order';
                }
            }
        }
        return (this.count > 0) ? this.grades.join(', ') : '-';
    }
}
