const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
});
const Grade = require('./Grade');
const { colorizeConsole } = require('./helper');

let grades = [];

console.log(colorizeConsole(35, '\nSelamat datang di program penilaian mahasiswa! 👋'));

readline.question(colorizeConsole(34, '\nMasukkan nama penilaian (Kuis, UTS, UAS, dll): '), (gradeName) => {
    console.log(colorizeConsole(36, 'Inputkan nilai dan ketik "q" jika sudah selesai\n'));
    readline.on('line', (grade) => {
        grade = grade.trim().toLowerCase();
        if (grade === 'q') {
            readline.close();
            return;
        }

        if (isNaN(grade) || grade === '') {
            console.log(colorizeConsole(31, '❌ Input hanya menerima angka!'));
            return;
        }
        if (grade < 0) {
            console.log(colorizeConsole(31, '❌ Input tidak boleh kurang dari 0!'));
            return;
        }
        if (grade > 100) {
            console.log(colorizeConsole(31, '❌ Input tidak boleh lebih dari 100!'));
            return;
        }

        grades.push(+grade);
        console.log(colorizeConsole(32, '✔ Nilai berhasil ditambahkan!'));
    }).on('close', () => {
        const newGrades = new Grade(gradeName, grades);
        console.log(colorizeConsole(36, `\nSelesai memasukkan nilai, cari outputnya (${newGrades.name}):\n`));

        console.log(`${colorizeConsole(34, 'Jumlah mahasiswa                        :')} ${colorizeConsole(33, newGrades.count)}`);

        console.log(`${colorizeConsole(34, 'Nilai tertinggi                         :')} ${colorizeConsole(33, newGrades.max())}`);
        console.log(`${colorizeConsole(34, 'Nilai terendah                          :')} ${colorizeConsole(33, newGrades.min())}`);
        console.log(`${colorizeConsole(34, 'Nilai rata-rata                         :')} ${colorizeConsole(33, newGrades.avg())}`);

        console.log(`${colorizeConsole(34, 'Jumlah mahasiswa yang lulus             :')} ${colorizeConsole(33, newGrades.pass())}`);
        console.log(`${colorizeConsole(34, 'Jumlah mahasiswa yang tidak lulus       :')} ${colorizeConsole(33, newGrades.fail())}`);

        console.log(`${colorizeConsole(34, 'Urutan nilai dari terendah ke tertinggi :')} ${colorizeConsole(33, newGrades.sort('asc'))}`);
        console.log(`${colorizeConsole(34, 'Urutan nilai dari tertinggi ke terendah :')} ${colorizeConsole(33, newGrades.sort('desc'))}`);

        console.log(colorizeConsole(35, '\n🙂 Terima kasih telah menggunakan program ini! 🙂'));
    });
});
